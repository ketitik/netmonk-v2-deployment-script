#!/bin/sh
cd message-broker/nats
./up.sh

echo "Waiting 5 seconds for message broker service setup initialization"
sleep 5

cd ../../db
./up.sh

echo "Waiting 5 seconds for databases service setup initialization"
sleep 5

cd ../kong
./up.sh

cd ../backend
./up.sh

cd ../agent
./up.sh

cd ../few
./up.sh
