print("Begin Init Script.")
db = db.getSiblingDB('nm_db');
db.probes_registrant.insert({
    _id : ObjectId("59e6edcec79b098237220485"),
    probe_name : "default.network.probe",
    location : "",
    remote_ip : "",
    key : "secret",
    type: "network",
    scarpe_interval : 300,
    message_broker_address : "message-broker.internal.netmonk.id:9092",
    snmp_version : 2,
    snmp_community : "public",
    snmp_username : "",
    snmp_password : "",
    snmp_timeout : 5,
    security_level : "",
    auth_protocol : "",
    priv_protocol : "",
    priv_password : "",
    user_group_owner_id: 1,
    user_group_ids: [1],
    customer_id: 1,
    created_at : new Date(),
    updated_at : new Date()
});
db.probes_registrant.insert({
    _id : ObjectId("61498aec9a5de7336aad9607"),
    probe_name : "default.web.probe",
    location : "",
    remote_ip : "",
    key : "secret",
    type: "website",
    scarpe_interval : 60,
    message_broker_address : "message-broker.internal.netmonk.id:9092",
    user_group_owner_id: 1,
    user_group_ids: [1],
    customer_id: 1,
    created_at : new Date(),
    updated_at : new Date()
});
db.location.insert({
    _id: ObjectId("61397caafb5c7e0001399ee2"),
    location: "Default Location"
});
db.createUser({
    user: 'nmuser247',
   pwd: 'nmpass247',
   roles: [{ role: 'readWrite', db:'nm_db'}]
});
db.node_inventory.createIndex({"probe_id": 1});
db.node_inventory.createIndex({"user_group_id": 1});
db.node_inventory.createIndex({"user_group_inventory_id": 1});
db.website_inventory.createIndex({"probe_id": 1});
db.website_inventory.createIndex({"user_group_id": 1});
db.website_inventory.createIndex({"user_group_inventory_id": 1});
db.server_inventory.createIndex({"type": 1});
db.server_inventory.createIndex({"user_group_id": 1});
db.server_inventory.createIndex({"user_group_inventory_id": 1});
print("DONE.")
