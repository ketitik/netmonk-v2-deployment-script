CREATE EXTENSION pgcrypto;

CREATE TABLE "customers" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "customer_id" varchar,
  "name" varchar NOT NULL,
  "address" varchar NOT NULL,
  "phone" varchar NOT NULL,
  "email" varchar UNIQUE NOT NULL,
  "image_url" varchar,
  "broker" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "quotas" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "customer_id" bigint,
  "node" int,
  "probe" int,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "user_groups" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "customer_id" bigint,
  "parent_id" bigint,
  "code" varchar,
  "name" varchar NOT NULL,
  "description" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "users" (
  "id" varchar PRIMARY KEY NOT NULL,
  "user_group_id" bigint NOT NULL,
  "username" varchar NOT NULL,
  "email" varchar,
  "password" varchar,
  "roles" varchar NOT NULL,
  "description" varchar,
  "failed_login_attempt" int DEFAULT 0,
  "login_time" timestamp,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "password_reset_tokens" (
  "user_id" varchar UNIQUE NOT NULL,
  "token" varchar NOT NULL,
  "expired_at" timestamp NOT NULL,
  "failed_verify_attempt" int DEFAULT 0,
  "verify_time" timestamp,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "modules" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "name" varchar NOT NULL
);

CREATE TABLE "inventory_types" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "module_id" bigint NOT NULL,
  "name" varchar NOT NULL
);

CREATE TABLE "user_group_inventories" (
  "id" varchar PRIMARY KEY NOT NULL,
  "user_group_id" bigint NOT NULL,
  "inventory_type_id" bigint NOT NULL,
  "inventory_id" varchar NOT NULL,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "tags" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_id" bigint NOT NULL,
  "name" varchar NOT NULL
);

CREATE TABLE "tags_user_group_inventories" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "tag_id" bigint NOT NULL,
  "user_group_inventory_id" varchar
);

CREATE TABLE "device_statuses" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "ip_management" varchar NOT NULL,
  "hostname" varchar,
  "node_inventory_name" varchar,
  "data_general_node" jsonb,
  "scrape_interval" integer NOT NULL,
  "data_icmp" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "link_utilizations" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "ip_management" varchar NOT NULL,
  "hostname" varchar,
  "node_inventory_name" varchar,
  "interface_name" varchar NOT NULL,
  "max_capacity" bigint,
  "scrape_interval" integer NOT NULL,
  "data" jsonb,
  "monitoring_status" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "toggled_at" timestamp
);

CREATE TABLE "monitoring_history" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "utilization_id" bigint NOT NULL,
  "utilization_type" varchar NOT NULL,
  "status" varchar,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "cpu_utilizations" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "ip_management" varchar NOT NULL,
  "hostname" varchar,
  "node_inventory_name" varchar,
  "cpu_index" varchar NOT NULL,
  "scrape_interval" integer NOT NULL,
  "data" jsonb,
  "monitoring_status" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "toggled_at" timestamp
);

CREATE TABLE "memory_utilizations" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "ip_management" varchar NOT NULL,
  "hostname" varchar,
  "node_inventory_name" varchar,
  "memory_index" varchar NOT NULL,
  "scrape_interval" integer NOT NULL,
  "data" jsonb,
  "monitoring_status" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "toggled_at" timestamp
);

CREATE TABLE "storage_utilizations" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "ip_management" varchar NOT NULL,
  "hostname" varchar,
  "node_inventory_name" varchar,
  "storage_index" varchar NOT NULL,
  "scrape_interval" integer NOT NULL,
  "data" jsonb,
  "monitoring_status" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "toggled_at" timestamp
);

CREATE TABLE "diskio_utilizations" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "ip_management" varchar NOT NULL,
  "hostname" varchar,
  "node_inventory_name" varchar,
  "disk_name" varchar NOT NULL,
  "scrape_interval" integer NOT NULL,
  "data" jsonb,
  "monitoring_status" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "toggled_at" timestamp
);

CREATE TABLE "website_statuses" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "website_id" varchar NOT NULL,
  "name" varchar NOT NULL,
  "url" varchar,
  "probe_id" varchar NOT NULL,
  "probe_name" varchar NOT NULL,
  "check_interval" integer,
  "threshold" float NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "website_pause_history" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "website_id" varchar NOT NULL,
  "is_paused" boolean,
  "reason" varchar,
  "created_at" timestamp
);

CREATE TABLE "server_system_cpus" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_system_uptimes" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "installed_at" timestamp,
  "restarted_at" timestamp,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_informations" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "platform" jsonb,
  "cpu" jsonb,
  "memory" jsonb,
  "file_systems" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_cpus" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "name" varchar,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_mems" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_disks" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "device" varchar,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_diskios" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "name" varchar,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_nets" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "interface" varchar,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_procstats" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "process_name" varchar,
  "pid" integer,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_docker_host" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "engine_host" varchar,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_docker_cpu" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "engine_host" varchar,
  "container_image" varchar,
  "container_service_name" varchar,
  "container_config_path" varchar,
  "container_name" varchar,
  "container_version" varchar,
  "container_status" varchar,
  "check_interval" integer NOT NULL,
  "cpu" varchar,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_docker_net" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "engine_host" varchar,
  "container_image" varchar,
  "container_service_name" varchar,
  "container_config_path" varchar,
  "container_name" varchar,
  "container_version" varchar,
  "container_status" varchar,
  "check_interval" integer NOT NULL,
  "network" varchar,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_docker_mem" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "engine_host" varchar,
  "container_image" varchar,
  "container_service_name" varchar,
  "container_config_path" varchar,
  "container_name" varchar,
  "container_version" varchar,
  "container_status" varchar,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_docker_blkio" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "engine_host" varchar,
  "container_image" varchar,
  "container_service_name" varchar,
  "container_config_path" varchar,
  "container_name" varchar,
  "container_version" varchar,
  "container_status" varchar,
  "check_interval" integer NOT NULL,
  "device" varchar,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "server_docker_status" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "server_id" varchar NOT NULL,
  "server_name" varchar,
  "engine_host" varchar,
  "container_image" varchar,
  "container_service_name" varchar,
  "container_config_path" varchar,
  "container_name" varchar,
  "container_version" varchar,
  "container_status" varchar,
  "check_interval" integer NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "uptime_sla_daily" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "ip_management" varchar NOT NULL,
  "data" jsonb,
  "reported_time" timestamp NOT NULL,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "icmp_sla_daily" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "ip_management" varchar NOT NULL,
  "data" jsonb,
  "reported_time" timestamp NOT NULL,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "link_daily" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "ip_management" varchar NOT NULL,
  "interface_name" varchar NOT NULL,
  "data" jsonb,
  "reported_time" timestamp NOT NULL,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "alert_notifications" (
  "id" BIGSERIAL PRIMARY KEY,
  "user_group_id" bigint NOT NULL,
  "name" varchar NOT NULL,
  "is_enabled" boolean NOT NULL,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "alert_rule_groups" (
  "id" BIGSERIAL PRIMARY KEY,
  "name" varchar,
  "created_at" timestamp,
  "updated_at" timestamp
);

CREATE TABLE "alert_rules" (
  "id" BIGSERIAL PRIMARY KEY,
  "alert_rule_group_id" bigint NOT NULL,
  "name" varchar NOT NULL,
  "type" varchar NOT NULL,
  "url" varchar NOT NULL,
  "fields" jsonb,
  "subject" varchar,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "notifications" (
  "id" BIGSERIAL PRIMARY KEY,
  "type" varchar NOT NULL,
  "label" varchar NOT NULL,
  "fields" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "alert_notification_user_groups" (
  "id" BIGSERIAL PRIMARY KEY,
  "alert_notification_id" bigint NOT NULL,
  "user_group_id" bigint NOT NULL,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "enabled_alert_rules" (
  "id" BIGSERIAL PRIMARY KEY,
  "alert_notification_id" bigint NOT NULL,
  "alert_rule_id" bigint NOT NULL,
  "data" jsonb,
  "is_enabled" boolean,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "enabled_notifications" (
  "id" BIGSERIAL PRIMARY KEY,
  "alert_notification_id" bigint NOT NULL,
  "notification_id" bigint NOT NULL,
  "data" jsonb,
  "created_at" timestamp,
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE INDEX "ug_code_unidx" ON "user_groups" ("code");

CREATE UNIQUE INDEX "u_username_unidx" ON "users" ("username");

CREATE UNIQUE INDEX "u_email_unidx" ON "users" ("email");

CREATE INDEX "ugi_inventory_id_idx" ON "user_group_inventories" ("inventory_id");

CREATE UNIQUE INDEX "ds_ugi_ipmgmt_unidx" ON "device_statuses" ("user_group_inventory_id", "ip_management");

CREATE INDEX "ds_ip_management_idx" ON "device_statuses" ("ip_management");

CREATE UNIQUE INDEX "lu_ugi_ipmgmt_ifname_unidx" ON "link_utilizations" ("user_group_inventory_id", "ip_management", "interface_name");

CREATE INDEX "lu_ip_management_idx" ON "link_utilizations" ("ip_management");

CREATE INDEX "lu_interface_name_idx" ON "link_utilizations" ("interface_name");

CREATE INDEX "mh_utilization_idx" ON "monitoring_history" ("utilization_id");

CREATE UNIQUE INDEX "cu_ugi_ipmgmt_cpuindex_unidx" ON "cpu_utilizations" ("user_group_inventory_id", "ip_management", "cpu_index");

CREATE INDEX "cu_ip_management_idx" ON "cpu_utilizations" ("ip_management");

CREATE INDEX "cu_cpu_index_idx" ON "cpu_utilizations" ("cpu_index");

CREATE UNIQUE INDEX "mu_ugi_ipmgmt_memoryindex_unidx" ON "memory_utilizations" ("user_group_inventory_id", "ip_management", "memory_index");

CREATE INDEX "mu_ip_management_idx" ON "memory_utilizations" ("ip_management");

CREATE INDEX "mu_memory_index_idx" ON "memory_utilizations" ("memory_index");

CREATE UNIQUE INDEX "su_ugi_ipmgmt_storageindex_unidx" ON "storage_utilizations" ("user_group_inventory_id", "ip_management", "storage_index");

CREATE INDEX "su_ip_management_idx" ON "storage_utilizations" ("ip_management");

CREATE INDEX "su_storage_index_idx" ON "storage_utilizations" ("storage_index");

CREATE UNIQUE INDEX "du_ugi_ipmgmt_diskname_unidx" ON "diskio_utilizations" ("user_group_inventory_id", "ip_management", "disk_name");

CREATE INDEX "du_ip_management_idx" ON "diskio_utilizations" ("ip_management");

CREATE INDEX "du_disk_name_idx" ON "diskio_utilizations" ("disk_name");

CREATE UNIQUE INDEX "ws_ugi_webid_unidx" ON "website_statuses" ("user_group_inventory_id", "website_id");

CREATE INDEX "ws_website_id_idx" ON "website_statuses" ("website_id");

CREATE UNIQUE INDEX "wph_ugi_webid_unidx" ON "website_pause_history" ("user_group_inventory_id", "website_id");

CREATE INDEX "wph_website_id_idx" ON "website_pause_history" ("website_id");

CREATE UNIQUE INDEX "lssc_ugi_server_id_unidx" ON "server_system_cpus" ("user_group_inventory_id", "server_id");

CREATE INDEX "lssc_server_id_idx" ON "server_system_cpus" ("server_id");

CREATE UNIQUE INDEX "lssu_unidx" ON "server_system_uptimes" ("user_group_inventory_id", "server_id");

CREATE INDEX "lssu_server_id_idx" ON "server_system_uptimes" ("server_id");

CREATE UNIQUE INDEX "lsi_unidx" ON "server_informations" ("user_group_inventory_id", "server_id");

CREATE INDEX "lsi_server_id_idx" ON "server_informations" ("server_id");

CREATE UNIQUE INDEX "lsc_unidx" ON "server_cpus" ("user_group_inventory_id", "server_id", "name");

CREATE INDEX "lsc_server_id_idx" ON "server_cpus" ("server_id");

CREATE UNIQUE INDEX "lsm_unidx" ON "server_mems" ("user_group_inventory_id", "server_id");

CREATE INDEX "lsm_server_id_idx" ON "server_mems" ("server_id");

CREATE UNIQUE INDEX "lsd_unidx" ON "server_disks" ("user_group_inventory_id", "server_id", "device");

CREATE INDEX "lsd_server_id_idx" ON "server_disks" ("server_id");

CREATE UNIQUE INDEX "lsdi_unidx" ON "server_diskios" ("user_group_inventory_id", "server_id", "name");

CREATE INDEX "lsdi_server_id_idx" ON "server_diskios" ("server_id");

CREATE UNIQUE INDEX "lsn_unidx" ON "server_nets" ("user_group_inventory_id", "server_id", "interface");

CREATE INDEX "lsn_server_id_idx" ON "server_nets" ("server_id");

CREATE UNIQUE INDEX "lsp_unidx" ON "server_procstats" ("user_group_inventory_id", "server_id", "process_name", "pid");

CREATE INDEX "lsp_server_id_idx" ON "server_procstats" ("server_id");

CREATE INDEX "lsp_process_name_idx" ON "server_procstats" ("process_name");

CREATE INDEX "lsp_pid_idx" ON "server_procstats" ("pid");

CREATE UNIQUE INDEX "lsdh_unidx" ON "server_docker_host" ("user_group_inventory_id", "server_id");

CREATE INDEX "lsdh_server_id_idx" ON "server_docker_host" ("server_id");

CREATE UNIQUE INDEX "lsdc_unidx" ON "server_docker_cpu" ("user_group_inventory_id", "server_id", "container_name", "cpu");

CREATE INDEX "lsdc_server_id_idx" ON "server_docker_cpu" ("server_id");

CREATE UNIQUE INDEX "lsdn_unidx" ON "server_docker_net" ("user_group_inventory_id", "server_id", "container_name", "network");

CREATE INDEX "lsdn_server_id_idx" ON "server_docker_net" ("server_id");

CREATE UNIQUE INDEX "lsdm_unidx" ON "server_docker_mem" ("user_group_inventory_id", "server_id", "container_name");

CREATE INDEX "lsdm_server_id_idx" ON "server_docker_mem" ("server_id");

CREATE UNIQUE INDEX "lsdb_unidx" ON "server_docker_blkio" ("user_group_inventory_id", "server_id", "container_name", "device");

CREATE INDEX "lsdb_server_id_idx" ON "server_docker_blkio" ("server_id");

CREATE UNIQUE INDEX "lsds_unidx" ON "server_docker_status" ("user_group_inventory_id", "server_id", "container_name");

CREATE INDEX "lsds_server_id_idx" ON "server_docker_status" ("server_id");

CREATE UNIQUE INDEX "usd_ugi_ipmgmt_unidx" ON "uptime_sla_daily" ("user_group_inventory_id", "ip_management");

CREATE UNIQUE INDEX "isd_ugi_ipmgmt_unidx" ON "icmp_sla_daily" ("user_group_inventory_id", "ip_management");

CREATE UNIQUE INDEX "isd_ugi_ipmgmt_ifname_unidx" ON "link_daily" ("user_group_inventory_id", "ip_management", "interface_name");

ALTER TABLE "quotas" ADD CONSTRAINT "q_customer_fk" FOREIGN KEY ("customer_id") REFERENCES "customers" ("id") ON DELETE CASCADE;

ALTER TABLE "user_groups" ADD CONSTRAINT "ug_customer_fk" FOREIGN KEY ("customer_id") REFERENCES "customers" ("id") ON DELETE RESTRICT;

ALTER TABLE "user_groups" ADD CONSTRAINT "ug_user_groups_fk" FOREIGN KEY ("parent_id") REFERENCES "user_groups" ("id") ON DELETE RESTRICT;

ALTER TABLE "users" ADD CONSTRAINT "u_users_group_fk" FOREIGN KEY ("user_group_id") REFERENCES "user_groups" ("id") ON DELETE RESTRICT;

ALTER TABLE "password_reset_tokens" ADD CONSTRAINT "prt_users_fk" FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE;

ALTER TABLE "inventory_types" ADD CONSTRAINT "it_module_fk" FOREIGN KEY ("module_id") REFERENCES "modules" ("id") ON DELETE RESTRICT;

ALTER TABLE "user_group_inventories" ADD CONSTRAINT "ugi_user_groups_fk" FOREIGN KEY ("user_group_id") REFERENCES "user_groups" ("id") ON DELETE RESTRICT;

ALTER TABLE "user_group_inventories" ADD CONSTRAINT "ugi_inventory_types_fk" FOREIGN KEY ("inventory_type_id") REFERENCES "inventory_types" ("id") ON DELETE RESTRICT;

ALTER TABLE "tags" ADD CONSTRAINT "t_user_groups_fk" FOREIGN KEY ("user_group_id") REFERENCES "user_groups" ("id") ON DELETE RESTRICT;

ALTER TABLE "tags_user_group_inventories" ADD CONSTRAINT "tugi_tags_fk" FOREIGN KEY ("tag_id") REFERENCES "tags" ("id") ON DELETE RESTRICT;

ALTER TABLE "tags_user_group_inventories" ADD CONSTRAINT "tugi_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE RESTRICT;

ALTER TABLE "device_statuses" ADD CONSTRAINT "ds_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "link_utilizations" ADD CONSTRAINT "lu_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "cpu_utilizations" ADD CONSTRAINT "cu_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "memory_utilizations" ADD CONSTRAINT "mu_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "storage_utilizations" ADD CONSTRAINT "su_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "diskio_utilizations" ADD CONSTRAINT "du_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "website_statuses" ADD CONSTRAINT "ws_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "website_pause_history" ADD CONSTRAINT "wph_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_system_cpus" ADD CONSTRAINT "lssc_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_system_uptimes" ADD CONSTRAINT "lssu_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_informations" ADD CONSTRAINT "lsi_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_cpus" ADD CONSTRAINT "lsc_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_mems" ADD CONSTRAINT "lsm_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_disks" ADD CONSTRAINT "lsd_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_diskios" ADD CONSTRAINT "lsdi_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_nets" ADD CONSTRAINT "lsn_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_procstats" ADD CONSTRAINT "lsp_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_docker_host" ADD CONSTRAINT "lsdh_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_docker_cpu" ADD CONSTRAINT "lsdc_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_docker_net" ADD CONSTRAINT "lsdn_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_docker_mem" ADD CONSTRAINT "lsdm_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_docker_blkio" ADD CONSTRAINT "lsdb_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "server_docker_status" ADD CONSTRAINT "lsds_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "uptime_sla_daily" ADD CONSTRAINT "usd_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "icmp_sla_daily" ADD CONSTRAINT "isd_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "link_daily" ADD CONSTRAINT "ld_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;

ALTER TABLE "alert_notifications" ADD CONSTRAINT "an_user_group_fk" FOREIGN KEY ("user_group_id") REFERENCES "user_groups" ("id") ON DELETE RESTRICT;

ALTER TABLE "alert_rules" ADD CONSTRAINT "ar_alert_rule_groups_fk" FOREIGN KEY ("alert_rule_group_id") REFERENCES "alert_rule_groups" ("id") ON DELETE CASCADE;

ALTER TABLE "alert_notification_user_groups" ADD CONSTRAINT "anug_alert_notifications_fk" FOREIGN KEY ("alert_notification_id") REFERENCES "alert_notifications" ("id") ON DELETE CASCADE;

ALTER TABLE "alert_notification_user_groups" ADD CONSTRAINT "anug_user_groups_fk" FOREIGN KEY ("user_group_id") REFERENCES "user_groups" ("id") ON DELETE CASCADE;

ALTER TABLE "enabled_alert_rules" ADD CONSTRAINT "ear_alert_notifications_fk" FOREIGN KEY ("alert_notification_id") REFERENCES "alert_notifications" ("id") ON DELETE CASCADE;

ALTER TABLE "enabled_alert_rules" ADD CONSTRAINT "ear_alert_rules_fk" FOREIGN KEY ("alert_rule_id") REFERENCES "alert_rules" ("id") ON DELETE CASCADE;

ALTER TABLE "enabled_notifications" ADD CONSTRAINT "en_alert_notifications_fk" FOREIGN KEY ("alert_notification_id") REFERENCES "alert_notifications" ("id") ON DELETE CASCADE;

ALTER TABLE "enabled_notifications" ADD CONSTRAINT "en_notifications_fk" FOREIGN KEY ("notification_id") REFERENCES "notifications" ("id") ON DELETE CASCADE;