CREATE TABLE "device_qos_metrics" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_group_inventory_id" varchar,
  "ip_management" varchar NOT NULL,
  "node_inventory_name" varchar,
  "scrape_interval" integer NOT NULL,
  "jitter" float,
  "latency_min" float,
  "latency_max" float,
  "latency_avg" float,
  "packet_sent" int,
  "packet_received" int,
  "created_at" timestamp,
  "updated_at" timestamp
);
CREATE UNIQUE INDEX "dqm_ugi_ipmgmt_unidx" ON "device_qos_metrics" ("user_group_inventory_id", "ip_management");
CREATE INDEX "dqm_ip_management_idx" ON "device_qos_metrics" ("ip_management");
ALTER TABLE "device_qos_metrics" ADD CONSTRAINT "dqm_user_group_inventories_fk" FOREIGN KEY ("user_group_inventory_id") REFERENCES "user_group_inventories" ("id") ON DELETE CASCADE;