CREATE TABLE "user_otps" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "user_id" varchar UNIQUE,
  "otp" varchar,
  "failed_verify_attempt" int DEFAULT 0,
  "verify_time" timestamp,
  "expired_at" timestamp,
  "mail_sent" int DEFAULT 0,
  "created_at" timestamp,
  "updated_at" timestamp
);

ALTER TABLE "user_otps" ADD CONSTRAINT "uo_users_fk" FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE;
