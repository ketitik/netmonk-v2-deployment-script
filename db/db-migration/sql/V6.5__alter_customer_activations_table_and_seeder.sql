ALTER TABLE customer_activations ADD CONSTRAINT unique_customer_id UNIQUE (customer_id);

INSERT INTO customer_activations (
    customer_id,
    status,
    type,
    reason_suspended,
    reason_deactivation,
    expired_at,
    deactivated_at,
    suspended_at,
    created_at
)
SELECT
    c.id AS customer_id,
    'active' AS status,
    CASE
        WHEN c.customer_id LIKE 'ID01%' THEN 'business'
        WHEN c.customer_id LIKE 'ID02%' THEN 'government'
        ELSE ''
    END AS type,
    '' AS reason_suspended,
    '' AS reason_deactivation,
    NULL AS expired_at,
    NULL AS deactivated_at,
    NULL AS suspended_at,
    c.created_at AS created_at
FROM customers c
ON CONFLICT (customer_id) DO NOTHING;
