CREATE TABLE "customer_activations" (
  "id" BIGSERIAL PRIMARY KEY NOT NULL,
  "customer_id" bigint,
  "status" varchar,
  "type" varchar,
  "reason_suspended" varchar,
  "reason_deactivation" varchar,
  "expired_at" timestamp,
  "deactivated_at" timestamp,
  "suspended_at" timestamp,
  "created_at" timestamp,
  "updated_at" timestamp
);

ALTER TABLE "customer_activations" ADD CONSTRAINT "ca_customer_fk" FOREIGN KEY ("customer_id") REFERENCES "customers" ("id") ON DELETE CASCADE;

INSERT INTO customer_activations (customer_id, status, type, reason_suspended, reason_deactivation, expired_at, deactivated_at, suspended_at, created_at, updated_at)
VALUES (1, 'active', 'business', '', '', NULL, NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
