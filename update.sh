#!/bin/sh
cd db
docker compose pull

cd ../message-broker/nats
docker compose pull

cd ../kafka
docker compose pull

cd ../../agent
docker compose pull

cd ../kong
docker compose pull

cd ../backend
docker compose pull

cd ../few
docker compose pull

docker rmi -f $(docker images -f 'dangling=true' -q)
