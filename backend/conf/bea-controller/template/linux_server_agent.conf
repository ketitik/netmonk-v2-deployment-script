# Telegraf Configuration
#
# Telegraf is entirely plugin driven. All metrics are gathered from the
# declared inputs, and sent to the declared outputs.
#
# Plugins must be declared in here to be active.
# To deactivate a plugin, comment out the name and any variables.
#
# Use 'telegraf -config telegraf.conf -test' to see what metrics a config
# file would generate.
#
# Environment variables can be used anywhere in this config file, simply surround
# them with ${}. For strings the variable must be within quotes (ie, "${STR_VAR}"),
# for numbers and booleans they should be plain (ie, ${INT_VAR}, ${BOOL_VAR})


# Global tags can be specified here in key="value" format.
[global_tags]
# dc = "us-east-1" # will tag all metrics with dc=us-east-1
# rack = "1a"
## Environment variables can be used as tags, and throughout the config file
# user = "$USER"
user_group_inventory_id = "{{ .UserGroupInventoryID }}"

# Configuration for telegraf agent
[agent]
## Default data collection interval for all inputs
interval = "{{ .Interval }}"
## Rounds collection interval to 'interval'
## ie, if interval="10s" then always collect on :00, :10, :20, etc.
round_interval = true

## Telegraf will send metrics to outputs in batches of at most
## metric_batch_size metrics.
## This controls the size of writes that Telegraf sends to output plugins.
metric_batch_size = {{ .MetricBatchSize }}

## Maximum number of unwritten metrics per output.  Increasing this value
## allows for longer periods of output downtime without dropping metrics at the
## cost of higher maximum memory usage.
metric_buffer_limit = 10000

## Collection jitter is used to jitter the collection by a random amount.
## Each plugin will sleep for a random time within jitter before collecting.
## This can be used to avoid many plugins querying things like sysfs at the
## same time, which can have a measurable effect on the system.
collection_jitter = "0s"

## Default flushing interval for all outputs. Maximum flush_interval will be
## flush_interval + flush_jitter
flush_interval = "10s"
## Jitter the flush interval by a random amount. This is primarily to avoid
## large write spikes for users running a large number of telegraf instances.
## ie, a jitter of 5s and interval 10s means flushes will happen every 10-15s
flush_jitter = "0s"

## By default or when set to "0s", precision will be set to the same
## timestamp order as the collection interval, with the maximum being 1s.
##   ie, when interval = "10s", precision will be "1s"
##       when interval = "250ms", precision will be "1ms"
## Precision will NOT be used for service inputs. It is up to each individual
## service input to set the timestamp at the appropriate precision.
## Valid time units are "ns", "us" (or "µs"), "ms", "s".
precision = ""

## Log at debug level.
# debug = false
## Log only error level messages.
# quiet = false

## Log target controls the destination for logs and can be one of "file",
## "stderr" or, on Windows, "eventlog".  When set to "file", the output file
## is determined by the "logfile" setting.
logtarget = "file"

## Name of the file to be logged to when using the "file" logtarget.  If set to
## the empty string then logs are written to stderr.
logfile = "/var/log/netmonk-telegraf/netmonk-telegraf.log"

## The logfile will be rotated after the time interval specified.  When set
## to 0 no time based rotation is performed.  Logs are rotated only when
## written to, if there is no log activity rotation may be delayed.
# logfile_rotation_interval = "0d"

## The logfile will be rotated when it becomes larger than the specified
## size.  When set to 0 no size based rotation is performed.
# logfile_rotation_max_size = "0MB"

## Maximum number of rotated archives to keep, any older logs are deleted.
## If set to -1, no archives are removed.
# logfile_rotation_max_archives = 5

## Pick a timezone to use when logging or type 'local' for local time.
## Example: America/Chicago
# log_with_timezone = ""

## Override default hostname, if empty use os.Hostname()
hostname = "{{ .HostName }}"
## If set to true, do no set the "host" tag in the telegraf agent.
omit_hostname = false


###############################################################################
#                            OUTPUT PLUGINS                                   #
###############################################################################

{{ $netmonkAgent := .NetmonkAgent }}{{ $broker := .MessageBroker }}{{if eq $broker.Type "kafka"}}
# Configuration for the Kafka server to send metrics to
[[outputs.kafka]]
## Kafka topic for producer messages
topic = "agent.server"

## Netmonk Configuration
netmonk_host = "{{ $netmonkAgent.NetmonkHost }}"
netmonk_server_id = "{{ $netmonkAgent.NetmonkServerID }}"
netmonk_server_key = "{{ $netmonkAgent.NetmonkServerKey }}"

## Data format to output.
## Each data format has its own unique set of configuration options, read
## more about them here:
## https://github.com/influxdata/telegraf/blob/master/docs/DATA_FORMATS_OUTPUT.md  
data_format = "json"
{{else}}# Send telegraf measurements to NATS
[[outputs.nats]]
## Optional client name
name = "netmonk-agent"

## NATS subject for producer messages
subject = "agent.server"

## Netmonk Configuration
netmonk_host = "{{ $netmonkAgent.NetmonkHost }}"
netmonk_server_id = "{{ $netmonkAgent.NetmonkServerID }}"
netmonk_server_key = "{{ $netmonkAgent.NetmonkServerKey }}"

## Data format to output.
## Each data format has its own unique set of configuration options, read
## more about them here:
## https://github.com/influxdata/telegraf/blob/master/docs/DATA_FORMATS_OUTPUT.md
data_format = "json"
{{end}}

###############################################################################
#                            INPUT PLUGINS                                    #
###############################################################################

# Read metrics about system load & uptime
[[inputs.system]]
## Uncomment to remove deprecated metrics.
fielddrop = ["uptime_format"]

{{$cpu := .CPU -}}{{ if $cpu.Enabled -}}
# Read metrics about cpu usage
[[inputs.cpu]]
## Whether to report per-cpu stats or not
percpu = {{ $cpu.PerCPU }}
## Whether to report total system cpu stats or not
totalcpu = {{ $cpu.TotalCPU }}
## If true, collect raw CPU time metrics
collect_cpu_time = {{ $cpu.CollectCPUTime }}
## If true, compute and report the sum of all non-idle CPU states
report_active = {{ $cpu.ReportActive }}

{{ end -}}

{{ $disk := .Disk -}}{{ if $disk.Enabled -}}
# Read metrics about disk usage by mount point
[[inputs.disk]]
{{ if $disk.MountPoints }}## By default stats will be gathered for all mount points.
## Set mount_points will restrict the stats to only the specified mount points.
mount_points = [ {{ joinWithCustomSeparation $disk.MountPoints }} ]{{ end -}}
{{ if $disk.IgnoredFS }}
## Ignore mount points by filesystem type.
#ignore_fs = [ {{ joinWithCustomSeparation $disk.IgnoredFS }} ]
{{ end -}}
{{ end -}}

{{ $diskIO := .DiskIO }}{{ if $diskIO.Enabled }}
# Read metrics about disk IO by device
[[inputs.diskio]]
{{ if $diskIO.Devices }}## By default, telegraf will gather stats for all devices including
## disk partitions.
## Setting devices will restrict the stats to the specified devices.
devices = [ {{ joinWithCustomSeparation $diskIO.Devices }} ]
{{ end -}}
{{ end -}}

{{ $mem := .Mem }}{{ if $mem.Enabled }}
# Read metrics about memory usage
[[inputs.mem]]
# no configuration
{{ end -}}

{{ $net := .Net }}{{ if $net.Enabled }}
# Read metrics about network interface usage
[[inputs.net]]
## By default, telegraf gathers stats from any up interface (excluding loopback)
## Setting interfaces will tell it to gather these explicit interfaces,
## regardless of status.
{{ if $net.Interfaces }}
  interfaces = [ {{ joinWithCustomSeparation $net.Interfaces }} ]
{{ end -}}
## On linux systems telegraf also collects protocol stats.
## Setting ignore_protocol_stats to true will skip reporting of protocol metrics.
##
# ignore_protocol_stats = false
##
{{ end -}}

{{ $procstat := .Procstat }}{{ if $procstat.Enabled }}
# Monitor process cpu and memory usage
{{ range $pattern := $procstat.Patterns -}}
[[inputs.procstat]]
## pattern as argument for pgrep (ie, pgrep -f <pattern>)
pattern = "{{ $pattern }}"
## Method to use when finding process IDs.  Can be one of 'pgrep', or
## 'native'.  The pgrep finder calls the pgrep executable in the PATH while
## the native finder performs the search directly in a manor dependent on the
## platform.  Default is 'pgrep'
pid_finder = "pgrep"
{{ end -}}
{{ end -}}

{{ $container := .Container }}{{ if $container.Enabled }}
# Monitor container container metrics
[[inputs.docker]]
  ## Docker Endpoint
  ##   To use TCP, set endpoint = "tcp://[ip]:[port]"
  ##   To use environment variables (ie, docker-machine), set endpoint = "ENV"
  endpoint = "unix:///var/run/docker.sock"

  ## Set to true to collect Swarm metrics(desired_replicas, running_replicas)
  ## Note: configure this in one of the manager nodes in a Swarm cluster.
  ## configuring in multiple Swarm managers results in duplication of metrics.
  gather_services = false

  ## Only collect metrics for these containers. Values will be appended to
  ## container_name_include.
  ## Deprecated (1.4.0), use container_name_include
  # container_names = []

  ## Set the source tag for the metrics to the container ID hostname, eg first 12 chars
  source_tag = false

  {{ if $container.Names }}## Containers to include and exclude. Collect all if empty. Globs accepted.
  container_name_include = [ {{ joinWithCustomSeparation $container.Names }} ]
  {{ end -}}
  container_name_exclude = []

  ## Container states to include and exclude. Globs accepted.
  ## When empty only containers in the "running" state will be captured.
  container_state_include = ["created", "restarting", "running", "removing", "paused", "exited", "dead"]
  # container_state_exclude = []

  ## Timeout for docker list, info, and stats commands
  timeout = "5s"

  docker_label_include = []
  docker_label_exclude = []

  ## Which environment variables should we use as a tag
  tag_env = ["JAVA_HOME", "HEAP_SIZE"]
{{ end -}}

# Read metrics from one or more commands that can output to stdout
[[inputs.exec]]
# Commands array
  commands = [
    "bash /etc/netmonk-telegraf/scripts/cpu_agent.sh",
    "bash /etc/netmonk-telegraf/scripts/memory_agent.sh",
    "bash /etc/netmonk-telegraf/scripts/filesystem_agent.sh",
    "bash /etc/netmonk-telegraf/scripts/platform_agent.sh",
  ]

# Timeout for each command to complete.
timeout = "5s"

# measurement name suffix (for separating different commands)
# name_suffix = "_mycollector"

# Data format to consume.
# Each data format has its own unique set of configuration options, read
# more about them here:
# https://github.com/influxdata/telegraf/blob/master/docs/DATA_FORMATS_INPUT.md
data_format = "influx"
