## RELEASE v6.0.0 - 2023-09-11

* [FEATURE] Deployment Script: Netmonk V6 Support

## RELEASE v5.5.7 - 2022-11-04

* [BUGFIXING] Deployment Script: Error when installing linux server agent on ARM architecture CPU.
* [FEATURE] Deployment Script: Add .env environment variable.
* [HOTFIX] Deployment Script: Fixing database relations.

## RELEASE v5.5.6 - 2022-10-12

* [FEATURE] Network Monitoring: Add interface capacity field.
* [FEATURE] Service: Add update root password script and removed example node and example website.

## RELEASE v5.4.6 - 2022-10-12

* [FEATURE] Linux Server Monitoring: Add new container monitoring feature.

## RELEASE v5.3.6 - 2022-08-29

* [FEATURE] Network Monitoring: Adding several new field to reporting response API.

## RELEASE v5.2.6 - 2022-08-26

* [BUGFIX] UI Network Monitoring: Fixing misstype.

## RELEASE v5.2.5 - 2022-07-29

* [FEATURE] Add New subscription tracker service.
* [BUGFIXING] Message Broker: Fixing broker-pwd.sh script, adding additional file to save the generated password.

## RELEASE v5.1.5 - 2022-07-15

* [FEATURE] Network Monitoring: New reporting format (summary and daily).
* [FEATURE] Network Monitoring: Starred link with custom bandwidth.
* [BUGFIXING] Message Broker: Fixing NATS slow consumer.

## RELEASE v5.0.5 - 2022-06-24

* [BUGFIXING] Server Monitoring: Fixing duplicate memory data.
* [BUGFIXING] Server Monitoring: Fixing diskio list data not updated.

## RELEASE v5.0.4 - 2022-05-17

* [BUGFIXING] Network UI: Fixing Network Monitoring dashboard summary css issues.
* [BUGFIXING] Server Monitoring: Fixing not accessable reporting download data page.

## RELEASE v5.0.4 - 2022-05-17

* [BUGFIXING] Network UI: Fixing Network Monitoring dashboard summary css issues.
* [BUGFIXING] Server Monitoring: Fixing not accessable reporting download data page.

## RELEASE v5.0.3 - 2022-05-17

* [BUGFIXING] Netmonk UI: Handling UI browser assets caching.
* [ENHANCEMENT] Netmonk Agent: Improvement data security.

## RELEASE v5.0.2 - 2022-05-10

* [BUGFIXING] Netmonk Agent: Fixing agent connection issues.

## RELEASE v5.0.1 - 2022-04-30

* [BUGFIXING] Network Monitoring: Fixing miss storage data for Fortigate router.

## RELEASE v5.0.0 - 2022-04-28

* [ENHANCEMENT] UI: New Netmonk UI/UX.
* [FEATURE] Server Monitoring: New Netmonk Prime capability to monitor Linux Servers.
 