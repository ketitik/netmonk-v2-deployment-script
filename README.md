#Netmonk V2 - Deployment Script

## Netmonk Installation
Clone the repository
```bash
$ git clone https://yourusername@bitbucket.org/ketitik/netmonk-v2-deployment-script.git netmonk-v2-deployment-script && cd netmonk-v2-deployment-script
```
For the first time, run setup script.
```bash
$ ./setup.sh
``` 

To turn Netmonk Prime system up

* Uncomment these values in few/.env file

```bash
# FEW_HOST_CONTAINER_ADDRESS=http://localhost:8000
# NETWORK_MONITORING_ADDRESS=http://localhost:8000/net-ms
# WEBSITE_MONITORING_ADDRESS=http://localhost:8000/web-ms
# SERVER_MONITORING_ADDRESS=http://localhost:8000/server-ms
# GENERAL_MODULE_ADDRESS=http://localhost:8000/general-md
# BASE_API_URL=http://localhost:8000/api
# BASENAME_PATH=/
```

* Update these values in backend/conf/bea-controller/apollo-bea-controller.yaml file

```bash
# Change to netmonk client's VM subdomain or host domain/ip address
verify_api_address: "http://nm-bea-controller:8000" 
```

* Start Netmonk Prime system

```bash
# For Development
$ ./up.sh

# For Production
$ ./up.sh prod
```

## Access Netmonk Website
View website dashboard at http://localhost:8000

Default Access
* Username: root
* Passowrd: PassPass1


## Stop Netmonk Service
To turn the system down, execute one of these code:
```bash

# For Development
$ ./down.sh

# For Production
$ ./down.sh prod
```
If you want to skip the message broker from being down, you can run:

```bash
# For Development
$ ./down.sh . broker-skip

# For Production
$ ./down.sh prod broker-skip
```

## Restart All Netmonk Service
To restart the entire system, execute one of these code:

```bash
# For Development
$ ./restart.sh

# For Production
$ ./restart.sh prod
```

## To update the default message broker password  
Usage: ./broker-pwd.sh [options]
 - -c: client id

### example

```
$ ./broker-pwd.sh -c test-client-id
```

## To update the default root user password 
Usage: ./root-pwd.sh [options]
 - -p: new password

### example

```
$ ./root-pwd.sh -p NewSuperSecretPass
```