#!/bin/sh

while getopts p: flag
do
    case "${flag}" in
        p) password=${OPTARG};;
    esac
done

if [ -z "$password" ]; then
    echo "empty password"
    exit 1
fi

docker exec -it kong_api_gateway_kong_1 curl --location --request PUT "nm-bec-user:8080/api/user/change-password/5c7cad56dd6a163882c17149" --header "X-Consumer-Groups: root" --header "X-Consumer-Custom-Id: 5c7cad56dd6a163882c17149" --header "Content-Type: application/json" --data-raw "{\"password\": \"$password\"}"
