INSERT INTO public.routes (
  id, created_at, updated_at, protocols, 
  methods, hosts, paths, regex_priority, 
  strip_path, preserve_host, service_id, 
  name, snis, sources, destinations, 
  tags, https_redirect_status_code, 
  headers, path_handling, ws_id, request_buffering, 
  response_buffering
) 
VALUES 
  (
    '88cb83ac-1462-40c7-a045-2db66c0180c0', 
    '2023-10-19 06:41:28+00', '2023-10-19 06:41:54+00', 
    '{http,https}', '{GET,OPTIONS}', 
    NULL, '{/api/activity-logs}', 0, 
    false, false, '5f0d41f7-6a36-47fe-a96d-78f466074ecc', 
    NULL, NULL, NULL, NULL, NULL, 426, NULL, 
    'v1', '909a6f0d-67eb-4fd5-affb-47a59fafd5f3', 
    true, true
  );
INSERT INTO public.plugins (
  id, name, consumer_id, config, enabled, 
  created_at, route_id, service_id, 
  cache_key, protocols, tags, ws_id
) 
VALUES 
  (
    '256f0e99-f361-463c-9ac3-3f40f9eb9a72', 
    'cors', NULL, '{"headers": ["Authorization", "User-Agent", "Accept", "Accept-Version", "Content-Length", "Content-MD5", "Content-Type", "Date"], "max_age": 3600, "methods": ["GET", "POST", "PUT", "DELETE", "PATCH"], "origins": ["*"], "credentials": true, "exposed_headers": null, "preflight_continue": false}', 
    true, '2023-09-10 22:30:14+00', 
    '88cb83ac-1462-40c7-a045-2db66c0180c0', 
    NULL, 'plugins:cors:88cb83ac-1462-40c7-a045-2db66c0180c0::::909a6f0d-67eb-4fd5-affb-47a59fafd5f3', 
    '{grpc,grpcs,http,https}', NULL, 
    '909a6f0d-67eb-4fd5-affb-47a59fafd5f3'
  );
INSERT INTO public.plugins (
  id, name, consumer_id, config, enabled, 
  created_at, route_id, service_id, 
  cache_key, protocols, tags, ws_id
) 
VALUES 
  (
    '5f7284ac-db44-4d6e-97fd-12fd9252593f', 
    'acl', NULL, '{"deny": null, "allow": ["superadmin", "root"], "hide_groups_header": false}', 
    true, '2023-10-19 07:06:17+00', 
    '88cb83ac-1462-40c7-a045-2db66c0180c0', 
    NULL, 'plugins:acl:88cb83ac-1462-40c7-a045-2db66c0180c0::::909a6f0d-67eb-4fd5-affb-47a59fafd5f3', 
    '{grpc,grpcs,http,https}', NULL, 
    '909a6f0d-67eb-4fd5-affb-47a59fafd5f3'
  );
INSERT INTO public.plugins (
  id, name, consumer_id, config, enabled, 
  created_at, route_id, service_id, 
  cache_key, protocols, tags, ws_id
) 
VALUES 
  (
    'a9f4b9e9-95da-4b37-9c65-a1ad259572fb', 
    'oauth2', NULL, '{"pkce": "lax", "scopes": null, "anonymous": null, "provision_key": "OVh2K88gumGquZEc4oGRo9QwaAqquLwi", "mandatory_scope": false, "auth_header_name": "authorization", "hide_credentials": false, "token_expiration": 7200, "refresh_token_ttl": 1209600, "global_credentials": true, "reuse_refresh_token": false, "enable_implicit_grant": false, "enable_password_grant": true, "enable_authorization_code": false, "enable_client_credentials": false, "accept_http_if_already_terminated": false}', 
    true, '2023-10-19 06:53:20+00', 
    '88cb83ac-1462-40c7-a045-2db66c0180c0', 
    NULL, 'plugins:oauth2:88cb83ac-1462-40c7-a045-2db66c0180c0::::909a6f0d-67eb-4fd5-affb-47a59fafd5f3', 
    '{grpc,grpcs,http,https}', NULL, 
    '909a6f0d-67eb-4fd5-affb-47a59fafd5f3'
  );