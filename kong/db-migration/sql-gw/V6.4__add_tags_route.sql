INSERT INTO public.routes (
  id, created_at, updated_at, protocols,
  methods, hosts, paths, regex_priority,
  strip_path, preserve_host, service_id,
  name, snis, sources, destinations,
  tags, https_redirect_status_code,
  headers, path_handling, ws_id, request_buffering,
  response_buffering
)
VALUES
  (
    'c480ec8e-50e5-48e6-8c6e-5a21223a48fd', '2023-11-07 07:21:02+00', '2023-11-07 07:57:03+00', '{http,https}',
    '{GET,POST,PUT,DELETE,OPTIONS}', NULL, '{/api/tags}', 0,
    false, false, '5f0d41f7-6a36-47fe-a96d-78f466074ecc',
    NULL, NULL, NULL, NULL, NULL, 426,
    NULL, 'v1', '909a6f0d-67eb-4fd5-affb-47a59fafd5f3', NULL,
    NULL
  );

INSERT INTO public.plugins (
  id, name, consumer_id, config, enabled,
  created_at, route_id, service_id,
  cache_key, protocols, tags, ws_id
)
VALUES
  (
    'add901d9-a67e-479e-ad13-167aeaf7fe96',
    'cors', NULL, '{"headers": ["Authorization", "User-Agent", "Accept", "Accept-Version", "Content-Length", "Content-MD5", "Content-Type", "Date"], "max_age": 3600, "methods": ["GET", "POST", "PUT", "DELETE", "PATCH"], "origins": ["*"], "credentials": true, "preflight_continue": false}',
    true, '2018-08-29 06:52:33+00',
    'c480ec8e-50e5-48e6-8c6e-5a21223a48fd',
    NULL, 'plugins:cors:c480ec8e-50e5-48e6-8c6e-5a21223a48fd::::909a6f0d-67eb-4fd5-affb-47a59fafd5f3',
    '{grpc,grpcs,http,https}', NULL, '909a6f0d-67eb-4fd5-affb-47a59fafd5f3'
  );

INSERT INTO public.plugins (
  id, name, consumer_id, config,
  enabled, created_at, route_id, service_id,
  cache_key,
  protocols, tags, ws_id
)
VALUES
  (
    '4f629a6b-3e29-42d9-8a79-abec6a269bcf', 'oauth2', NULL, '{"pkce": "lax", "scopes": null, "anonymous": null, "provision_key": "4W8JnCDXG3WxkGPbwiSREmmPPtWqtP6P", "mandatory_scope": false, "auth_header_name": "authorization", "hide_credentials": false, "token_expiration": 7200, "refresh_token_ttl": 1209600, "global_credentials": true, "reuse_refresh_token": false, "enable_implicit_grant": false, "enable_password_grant": true, "enable_authorization_code": false, "enable_client_credentials": false, "accept_http_if_already_terminated": false}',
    true, '2023-10-19 06:53:20+00', 'c480ec8e-50e5-48e6-8c6e-5a21223a48fd', NULL,
    'plugins:oauth2:c480ec8e-50e5-48e6-8c6e-5a21223a48fd::::909a6f0d-67eb-4fd5-affb-47a59fafd5f3',
    '{grpc,grpcs,http,https}', NULL,
    '909a6f0d-67eb-4fd5-affb-47a59fafd5f3'
  );

INSERT INTO public.plugins (
  id, name, consumer_id, config, enabled,
  created_at, route_id, service_id,
  cache_key, protocols, tags, ws_id
)
VALUES
  (
    'e3051a67-34ec-41f3-b6d0-dc0861e7141d',
    'acl', NULL, '{"deny": null, "allow": ["user", "admin", "superadmin", "root"], "hide_groups_header": false}', true,
    '2023-10-19 07:06:17+00', 'c480ec8e-50e5-48e6-8c6e-5a21223a48fd', NULL,
    'plugins:acl:c480ec8e-50e5-48e6-8c6e-5a21223a48fd::::909a6f0d-67eb-4fd5-affb-47a59fafd5f3', '{grpc,grpcs,http,https}',
    NULL, '909a6f0d-67eb-4fd5-affb-47a59fafd5f3'
  );