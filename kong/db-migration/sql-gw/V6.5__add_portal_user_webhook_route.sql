INSERT INTO public.consumers (
  id, created_at, username, custom_id, 
  tags, ws_id
) 
VALUES 
  (
    'cca09c32-7921-45f7-980e-266d9ee54184' :: uuid, 
    '2024-04-18 08:02:43.000', 'portal_webhook', 
    NULL, NULL, '909a6f0d-67eb-4fd5-affb-47a59fafd5f3' :: uuid
  );
INSERT INTO public.routes (
  id, created_at, updated_at, "name", 
  service_id, protocols, methods, hosts, 
  paths, snis, sources, destinations, 
  regex_priority, strip_path, preserve_host, 
  tags, https_redirect_status_code, 
  headers, path_handling, ws_id, request_buffering, 
  response_buffering
) 
VALUES 
  (
    'b0beaf30-7174-48a4-9dc0-adb5a5a3336e' :: uuid, 
    '2024-04-18 08:34:29.000', '2024-04-18 08:34:37.000', 
    NULL, '5f0d41f7-6a36-47fe-a96d-78f466074ecc' :: uuid, 
    '{http,https}', '{POST,OPTIONS}', 
    NULL, '{/api/webhook/user}', NULL, 
    NULL, NULL, 0, false, false, NULL, 426, 
    NULL, 'v1', '909a6f0d-67eb-4fd5-affb-47a59fafd5f3' :: uuid, 
    true, true
  );
INSERT INTO public.plugins (
  id, created_at, "name", consumer_id, 
  service_id, route_id, config, enabled, 
  cache_key, protocols, tags, ws_id
) 
VALUES 
  (
    '1a6f9f40-4d22-466c-a4d8-3ba732a8ccb5' :: uuid, 
    '2024-04-18 08:36:37.000', 'acl', 
    NULL, NULL, 'b0beaf30-7174-48a4-9dc0-adb5a5a3336e' :: uuid, 
    '{"deny": null, "allow": ["superadmin"], "hide_groups_header": false}' :: jsonb, 
    true, 'plugins:acl:b0beaf30-7174-48a4-9dc0-adb5a5a3336e::::909a6f0d-67eb-4fd5-affb-47a59fafd5f3', 
    '{grpc,grpcs,http,https}', NULL, 
    '909a6f0d-67eb-4fd5-affb-47a59fafd5f3' :: uuid
  );
INSERT INTO public.plugins (
  id, created_at, "name", consumer_id, 
  service_id, route_id, config, enabled, 
  cache_key, protocols, tags, ws_id
) 
VALUES 
  (
    'a4235063-b39c-44ad-8557-2f174dec42b3' :: uuid, 
    '2024-04-18 08:35:14.000', 'basic-auth', 
    NULL, NULL, 'b0beaf30-7174-48a4-9dc0-adb5a5a3336e' :: uuid, 
    '{"anonymous": null, "hide_credentials": false}' :: jsonb, 
    true, 'plugins:basic-auth:b0beaf30-7174-48a4-9dc0-adb5a5a3336e::::909a6f0d-67eb-4fd5-affb-47a59fafd5f3', 
    '{grpc,grpcs,http,https}', NULL, 
    '909a6f0d-67eb-4fd5-affb-47a59fafd5f3' :: uuid
  );
INSERT INTO public.plugins (
  id, created_at, "name", consumer_id, 
  service_id, route_id, config, enabled, 
  cache_key, protocols, tags, ws_id
) 
VALUES 
  (
    '9107b3f4-d905-4c63-89c5-ec66262147f3' :: uuid, 
    '2024-04-18 09:31:16.000', 'cors', 
    NULL, NULL, 'b0beaf30-7174-48a4-9dc0-adb5a5a3336e' :: uuid, 
    '{"headers": ["Authorization", "User-Agent", "Accept", "Accept-Version", "Content-Length", "Content-MD5", "Content-Type", "Date", "webhook-event"], "max_age": 3600, "methods": ["GET", "POST", "PUT", "DELETE", "PATCH"], "origins": ["*"], "credentials": true, "exposed_headers": null, "preflight_continue": false}' :: jsonb, 
    true, 'plugins:cors:b0beaf30-7174-48a4-9dc0-adb5a5a3336e::::909a6f0d-67eb-4fd5-affb-47a59fafd5f3', 
    '{grpc,grpcs,http,https}', NULL, 
    '909a6f0d-67eb-4fd5-affb-47a59fafd5f3' :: uuid
  );
INSERT INTO public.basicauth_credentials (
  id, created_at, consumer_id, username, 
  "password", tags, ws_id
) 
VALUES 
  (
    '04d18da9-7d97-4df2-8d03-de5978e5d2d6' :: uuid, 
    '2024-04-18 08:04:35.000', 'cca09c32-7921-45f7-980e-266d9ee54184' :: uuid, 
    'portal_webhook', '99871e16acfe021067537c50ff696549819a5530', 
    NULL, '909a6f0d-67eb-4fd5-affb-47a59fafd5f3' :: uuid
  );
INSERT INTO public.acls (
  id, created_at, consumer_id, "group", 
  cache_key, tags, ws_id
) 
VALUES 
  (
    'b7012090-2246-4766-8d82-f2c472398aa5' :: uuid, 
    '2024-04-18 08:02:55.000', 'cca09c32-7921-45f7-980e-266d9ee54184' :: uuid, 
    'superadmin', 'acls:cca09c32-7921-45f7-980e-266d9ee54184:superadmin::::909a6f0d-67eb-4fd5-affb-47a59fafd5f3', 
    NULL, '909a6f0d-67eb-4fd5-affb-47a59fafd5f3' :: uuid
  );